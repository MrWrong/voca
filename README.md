# Voca

Practice lists that you need to learn by heart

## Introduction

When I was in school, I needed to learn a lot of foreign language vocabulary. A very simple program on my parent's MS-DOS PC was a great help. It didn't have prepared lists, so the first part of the training was typing in the words, in a simple comma separated file. While practising, it offered to use either column as the questions, using the other as answers, so I could practice the list both ways. While rehearsing, each word I got wrong was followed up both immediately and again later on, until I had gotten every word right at least once. At the end, the program offered to redo the ones I got wrong in the same manner.

There are many vocabulary tutors available on the internet, either as web sites and in app stores. In my opinion they all fail in some way. Many have the lists prepared, or are made for phones. To learn, you need focus and time, so smart phones are not suitable. Lastly, being "for free" and closed source means that these apps will collect data about you in some fashion, which means that your identity will be collected and traded.

## Usage

This python 3 script does exactly what my old MS-DOS tutor did. Put it somewhere in your path, like `/usr/local/bin/`. Create a file with an arbitrary name (such as `lesson.csv`) in a text editor (don't use a word processor) with contents like these:

```
one,een
house,huis
cup,kopje
saucer,schotel
```

Then start voca using:

```
$ voca.py lesson.csv
```

It will use the first column as questions and compare your anwers to the corresponding items in the second column. Add `-s` to swap the columns or `-r` to review all the questions and their answers before practising. If you prefer a different delimiter, use -d. Starting voca without any arguments will show a small help page.

## Copying

Voca is licensed under the GNU Public License (GPL), version 3. This effectively means that you can freely change and redistribute voca, but only with this exact license and all copyright messages intact.
