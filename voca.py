#!/usr/bin/python
#
# Voca
#
# Copyright (C) 2023 Diederick de Vries
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import csv
import random
import os
import sys
import argparse

def load_file(filename):
    thelist = []
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='\\')
        for row in reader:
            thelist.append(row)
    return thelist
        
def swap_list(thelist):
    for row in thelist:
        temp = row[1]
        row[1] = row[0]
        row[0] = temp
        
def practice(thelist):
    errors = []
    print (str(len(thelist)) + " rows loaded.")
    input("Press enter to start")

    random.shuffle(thelist)

    i = 0
    for row in thelist:
        os.system('clear')
        answer = input(row[0] + "? ")
        while (answer != row[1]):
            if (not row in errors):
                errors.append(row)
                thelist.insert(i+3, row)
            print ("Sorry, no: " + row[1]);
            input("Press enter...")
            os.system('clear')
            answer = input(row[0] + "? ")
        i = i + 1

    print (str(len(errors)) + " wrong out of " + str(len(thelist)-len(errors)))
    if (len(errors) > 0 and "n" != input ("Redo wrong answers? (J/n)")):
        practice(errors)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                        prog='voca.py',
                        description='Practices comma separated thelists until you know them by heart')
    parser.add_argument('filename')
    parser.add_argument('-s', '--swap', action='store_true', help='use second column of each row as questions')
    args = parser.parse_args()

    thelist = load_file(args.filename)
    
    if (args.swap):
        swap_list(thelist)
        
    practice(thelist)
    print ("You're done.")
